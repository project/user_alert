// $Id:

Drupal.behaviors.user_alert_get_message = function(context) {
	if ($('div#block-user_alert-user_alert').length > 0) {
		$.ajax({
		   type: "GET",
		   url: "/admin/user-alert/get-message",
		   success: function(data){
				$('div#block-user_alert-user_alert').html(data);
		   }
		 });
	}
};

function user_alert_close_message() {
	$.ajax({
	   type: "GET",
	   url: "/admin/user-alert/close-message",
	   success: function(){
			$('div#user-alert').fadeOut('slow');
	   }
	 });
}